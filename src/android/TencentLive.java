package com.yqs.mall.tencent;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.os.*;
import org.apache.cordova.*;
import android.view.WindowManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONTokener;

import com.tencent.rtmp.*;
import com.tencent.rtmp.ui.*;

public class TencentLive extends CordovaPlugin {

    private Context context;
    private Activity activity;
    private CordovaInterface cordova;
    private CordovaWebView cordovaWebView;
    private ViewGroup rootView;
    private WebView webView;
    private WebSettings settings;
    private CallbackContext callbackContext;

    private TXCloudVideoView videoView = null;
    private TXLivePusher mLivePusher = null;
    protected TXLivePushListenerImpl  mTXLivePushListener;
    private TXLivePlayer mLivePlayer = null;

    private TXLiveBase txLiveBase = null;
    protected String LiveKey;
    protected String LiveLicenseUrl;

    private String[] permissions = {
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA
    };

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        this.cordovaWebView = webView;
        this.cordova = cordova;
        this.activity = cordova.getActivity();
        this.context = this.activity.getApplicationContext();
        this.rootView = (ViewGroup) activity.findViewById(android.R.id.content);
        this.webView = (WebView) rootView.getChildAt(0);
        initTxLiveBase();

    }

    protected void initTxLiveBase() {
        if (this.LiveKey == null) {
            this.LiveKey = preferences.getString("LIVE_KEY", "");
        }
        if (this.LiveLicenseUrl == null) {
            this.LiveLicenseUrl = preferences.getString("LIVE_LICENSE_URL", "");
        }
        TXLiveBase.getInstance().setLicence(this.context, this.LiveLicenseUrl, this.LiveKey);
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArry of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into JavaScript.
     * @return True if the action was valid, false if not.
     */
    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {

        if (!hasPermisssion()) {
            requestPermissions(0);
        }
        this.callbackContext = callbackContext;

       if (action.equals("startPush")) {
            final String url = args.getString(0);
            return startPush(url, callbackContext);
        } else if (action.equals("pausePusher")) {
            if (mLivePusher == null) return false;
            mLivePusher.pausePusher();
            callbackContext.success("success");
            return true;
        } else if (action.equals("resumePusher")) {
            if (mLivePusher == null) return false;
            mLivePusher.resumePusher();
            callbackContext.success("success");
            return true;
        } else if (action.equals("stopPush")) {
            return stopPush(callbackContext);
        } else if (action.equals("startPlay")) {
            final String url = args.getString(0);
            final int playType = args.getInt(1);
            return startPlay(url, playType, callbackContext);
        } else if (action.equals("stopPlay")) {
            return stopPlay(callbackContext);
        } else if (action.equals("setVideoQuality")) {
            if (mLivePusher == null) return false;
            final int quality = args.getInt(0);
            final boolean isAdjustBitrate = args.getInt(1) == 1;
            mLivePusher.setVideoQuality(quality, isAdjustBitrate, false);
            callbackContext.success("success");
            return true;
        } else if (action.equals("setBeautyFilterDepth")) {
            if (mLivePusher == null) return false;   
            final int style = args.getInt(0);
            final int beautyLevel = args.getInt(1);
            final int whiteningLevel = args.getInt(2);
            final int ruddyLevel = args.getInt(3);
            mLivePusher.setBeautyFilter(style, beautyLevel, whiteningLevel, ruddyLevel);
            callbackContext.success("success");
            return true;
        } else if (action.equals("setFilter")) {
            return false;
        } else if (action.equals("switchCamera")) {
            if (mLivePusher == null) return false;
            mLivePusher.switchCamera();
            callbackContext.success("success");
            return true;
        } else if (action.equals("setMirror")) {
            if (mLivePusher == null) return false;
            final boolean enabled = args.getInt(0) == 1;
            mLivePusher.setMirror(enabled);
            callbackContext.success("success");
            return true;
        } else if (action.equals("turnOnFlashLight")) {
            if (mLivePusher == null) return false;
            final boolean enabled = args.getInt(0) == 1;
            mLivePusher.turnOnFlashLight(enabled);
            callbackContext.success("success");
            return true;
        } else if (action.equals("getMaxZoom")) {
            if (mLivePusher == null) return false;
            int zoom = mLivePusher.getMaxZoom();
            callbackContext.success(zoom);
            return true;
        } else if (action.equals("setZoom")) {
            if (mLivePusher == null) return false;
            final int zoomValue = args.getInt(0);
            mLivePusher.setZoom(zoomValue);
            callbackContext.success("success");
            return true;
        } else if (action.equals("setExposureCompensation")) {
            if (mLivePusher == null) return false;
            final float depth = (float) args.getDouble(0);
            mLivePusher.setExposureCompensation(depth);
            callbackContext.success("success");
            return true;
        } else if (action.equals("fixMinFontSize")) {
//            return stopRecord(callbackContext);
            return fixMinFontSize(callbackContext);
        }

        callbackContext.error("Undefined action: " + action);
        return false;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        String statusCode;
        switch (requestCode) {
            case 990:  // demoPush
                if (resultCode == 1) {
                    statusCode = "success";
                    callbackContext.success(statusCode);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 在当前 Activity 底部 UI 层注册一个 TXCloudVideoView 以供直播渲染
     */
    private void prepareVideoView() {
        if (videoView != null) return;
        // 通过 layout 文件插入 videoView
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        videoView = (TXCloudVideoView) layoutInflater.inflate(_R("layout", "layout_video"), null);
        // 设置 webView 透明
        videoView.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.FILL_PARENT,
                FrameLayout.LayoutParams.FILL_PARENT
        ));
        // 插入视图
        rootView.addView(videoView);
        videoView.setVisibility(View.VISIBLE);
        // 设置 webView 透明
        webView.setBackgroundColor(Color.TRANSPARENT);
        // 关闭 webView 的硬件加速（否则不能透明）
        webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        // 将 webView 提到顶层
        webView.bringToFront();
    }

    /**
     * 销毁 videoView
     */
    private void destroyVideoView() {
        if (videoView == null) return;
        videoView.onDestroy();
        rootView.removeView(videoView);
        videoView = null;
        // 把 webView 变回白色
        //webView.setBackgroundColor(Color.WHITE);
    }

    /**
     * 设置最小字号
     *
     * @param callbackContext
     * @return
     */
    private boolean fixMinFontSize(final CallbackContext callbackContext) {
        try {
            settings = ((WebView) cordovaWebView.getEngine().getView()).getSettings();
            settings.setMinimumFontSize(1);
            settings.setMinimumLogicalFontSize(1);
            callbackContext.success("success");
        } catch (Exception error) {
            callbackContext.error("10003");
            return false;
        }
        return true;
    }

    /**
     * 开始推流，并且在垫底的 videoView 显示视频
     * 会在当前对象上下文注册一个 TXLivePusher
     *
     * @param url             推流URL
     * @param callbackContext
     * @return
     */
    private boolean startPush(final String url, final CallbackContext callbackContext) {
        if (mLivePusher != null) {
            callbackContext.error("开启推流失败：pusher 已存在");
            return false;
        }
        // 准备 videoView，没有的话生成
        activity.runOnUiThread(new Runnable() {
            public void run() {
                prepareVideoView();
                activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                // 开始推流
                mLivePusher = new TXLivePusher(activity);
                TXLivePushConfig mLivePushConfig = new TXLivePushConfig();
                mLivePusher.setConfig(mLivePushConfig);
                // 将视频绑定到 videoView
                mLivePusher.startCameraPreview(videoView);
                mTXLivePushListener = new TXLivePushListenerImpl();
                mLivePusher.setPushListener(mTXLivePushListener);
                //mLivePusher.setBeautyFilter(0, 5, 3, 2);
                int ret = mLivePusher.startPusher(url);
                if (ret != 0) {    
                    // 停止摄像头预览
                    mLivePusher.stopCameraPreview(true);
                    callbackContext.error("推流失败，code:"+ret);
                }else{
                    callbackContext.success("success");
                }

            }
        });
        return true;
    }

    /**
     * 停止推流，并且注销 mLivePusher 对象
     *
     * @param callbackContext
     * @return
     */
    private boolean stopPush(final CallbackContext callbackContext) {
        if (mLivePusher == null) {
            callbackContext.error("停止推流失败：pusher 不存在");
            return false;
        }
        activity.runOnUiThread(new Runnable() {
            public void run() {
                // 停止摄像头预览
                mLivePusher.stopCameraPreview(true);
                // 停止推流
                mLivePusher.stopPusher();
                // 解绑 Listener
                mLivePusher.setPushListener(null);
                // 移除 pusher 引用
                mLivePusher = null;
                activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                // 销毁 videoView
                destroyVideoView();
                callbackContext.success("success");
            }
        });
        return true;
    }


    /**
     * 开始播放，在垫底的 videoView 显示视频
     * 会在当前对象上下文注册一个 TXLivePlayer
     *
     * @param url             播放URL
     * @param playType        播放类型，参见 TencentLive.js 相关的枚举定义
     * @param callbackContext
     * @return
     */
    private boolean startPlay(final String url, final int playType, final CallbackContext callbackContext) {
        if (mLivePlayer != null) {
            callbackContext.error("开启播放失败：player 已存在");
            return false;
        }
        // 准备 videoView，没有的话生成
        activity.runOnUiThread(new Runnable() {
            public void run() {
                prepareVideoView();
                // 开始推流
                mLivePlayer = new TXLivePlayer(activity);
                TXLivePushConfig mLivePushConfig = new TXLivePushConfig();
                // 将视频绑定到 videoView
                mLivePlayer.setPlayerView(videoView);
                mLivePlayer.startPlay(url, playType);
                callbackContext.success("success");
            }
        });
        return true;
    }

    /**
     * 停止推流，并且注销 mLivePlay 对象
     *
     * @param callbackContext
     * @return
     */
    private boolean stopPlay(final CallbackContext callbackContext) {
        if (mLivePlayer == null) {
            callbackContext.error("关闭播放失败：player 不存在");
            return false;
        }
        activity.runOnUiThread(new Runnable() {
            public void run() {
                // 停止播放
                mLivePlayer.stopPlay(true);
                // 销毁 videoView
                destroyVideoView();
                // 移除 pusher 引用
                mLivePlayer = null;
                callbackContext.success("success");
            }
        });
        return true;
    }

    /**
     * check application's permissions
     */
    public boolean hasPermisssion() {
        for (String p : permissions) {
            if (!PermissionHelper.hasPermission(this, p)) {
                return false;
            }
        }
        return true;
    }

    /**
     * We override this so that we can access the permissions variable, which no longer exists in
     * the parent class, since we can't initialize it reliably in the constructor!
     *
     * @param requestCode The code to get request action
     */
    public void requestPermissions(int requestCode) {
        PermissionHelper.requestPermissions(this, requestCode, permissions);
    }

    public void alert(String msg, String title) {
        new AlertDialog.Builder(this.activity)
                .setTitle(title)
                .setMessage(msg)//设置显示的内容
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {//添加确定按钮
                    @Override
                    public void onClick(DialogInterface dialog, int which) {//确定按钮的响应事件
                        // TODO Auto-generated method stub
//                        finish();
                    }
                }).show();//在按键响应事件中显示此对话框
    }

    public void alert(String msg) {
        alert(msg, "系统提示");
    }

    public int _R(String defType, String name) {
        return activity.getApplication().getResources().getIdentifier(
                name, defType, activity.getApplication().getPackageName());
    }

    private class TXLivePushListenerImpl implements ITXLivePushListener {
        
        @Override
        public void onPushEvent(final int event, final Bundle param) {
            if (event == TXLiveConstants.PUSH_ERR_OPEN_CAMERA_FAIL) {
                String msg = "[LivePusher] 推流失败[打开摄像头失败]";
                callbackContext.error( msg);
            } else if (event == TXLiveConstants.PUSH_ERR_OPEN_MIC_FAIL) {
                String msg = "[LivePusher] 推流失败[打开麦克风失败]";
                callbackContext.error( msg);
            } else if (event == TXLiveConstants.PUSH_ERR_NET_DISCONNECT || event == TXLiveConstants.PUSH_ERR_INVALID_ADDRESS) {
                String msg = "[LivePusher] 推流失败[网络断开]";
                callbackContext.error( msg);
            } else if (event == TXLiveConstants.PUSH_ERR_SCREEN_CAPTURE_START_FAILED) {
                String msg = "[LivePusher] 推流失败[录屏启动失败]";
                callbackContext.error( msg);
            }
        }

        @Override
        public void onNetStatus(Bundle status) {

        }
    }

}
