#import <Cordova/CDV.h>
#import "TXLiteAVSDK_Smart/TXLivePush.h"
#import "TXLiteAVSDK_Smart/TXLivePlayer.h"

@interface TencentLive : CDVPlugin

@property UIView* videoView;
@property TXLivePush* livePusher;
@property TXLivePlayer* livePlayer;
@property (nonatomic, strong) NSString *LiveKey;
@property (nonatomic, strong) NSString *LiveLicenseUrl;

- (void) prepareVideoView;
- (void) destroyVideoView;
- (void) startPush:(CDVInvokedUrlCommand*)command;
- (void) pausePusher:(CDVInvokedUrlCommand*)command;
- (void) resumePusher:(CDVInvokedUrlCommand*)command;
- (void) stopPush:(CDVInvokedUrlCommand*)command;
- (void) startPlay:(CDVInvokedUrlCommand*)command;
- (void) stopPlay:(CDVInvokedUrlCommand*)command;
- (void) setVideoQuality:(CDVInvokedUrlCommand*)command;
- (void) setBeautyFilterDepth:(CDVInvokedUrlCommand*)command;
- (void) setFilter:(CDVInvokedUrlCommand*)command;
- (void) switchCamera:(CDVInvokedUrlCommand*)command;
- (void) setMirror:(CDVInvokedUrlCommand*)command;
- (void) turnOnFlashLight:(CDVInvokedUrlCommand*)command;
- (void) getMaxZoom:(CDVInvokedUrlCommand*)command;
- (void) setZoom:(CDVInvokedUrlCommand*)command;
- (void) setExposureCompensation:(CDVInvokedUrlCommand*)command;
- (void) alert:(NSString*)message title:(NSString*)title;
- (void) alert:(NSString*)message;

@end