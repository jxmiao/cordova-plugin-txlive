#import "TencentLive.h"

#import "MainViewController.h"
#import <Cordova/CDV.h>

@implementation MainViewController(CDVViewController)
- (void) viewDidLoad {
    [super viewDidLoad];
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.opaque = NO;
}
@end


@implementation TencentLive

@synthesize videoView;
@synthesize livePusher;
@synthesize livePlayer;

- (void)pluginInitialize {
    
    NSString* LiveKey = [[self.commandDelegate settings] objectForKey:@"live_key"];
    NSString* LiveLicenseUrl = [[self.commandDelegate settings] objectForKey:@"live_license_url"];
    if (LiveKey && LiveLicenseUrl){
        [TXLiveBase setLicenceURL:LiveLicenseUrl key:LiveKey];
    }

}

- (void) prepareVideoView {
    if (self.videoView) return;
    self.videoView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    [self.webView.superview addSubview:self.videoView];
    [self.webView.superview bringSubviewToFront:self.webView];
}

- (void) destroyVideoView {
    if (!self.videoView) return;
    [self.videoView removeFromSuperview];
    self.videoView = nil;
}

- (void) startPush:(CDVInvokedUrlCommand*)command {
    if (self.livePusher) return;
    NSString* url = [command.arguments objectAtIndex:0];
    [self prepareVideoView];
    TXLivePushConfig* _config = [[TXLivePushConfig alloc] init];
    self.livePusher = [[TXLivePush alloc] initWithConfig: _config];
    [self.livePusher startPreview:videoView];
    UIApplication* app = [UIApplication sharedApplication];
  if (![app isIdleTimerDisabled]) {
    [app setIdleTimerDisabled:true];
  }
    int ret = [self.livePusher startPush:url];
    if (ret != 0) {
        NSString* code = [NSString stringWithFormat:@"%d",ret];
        [self failWithCallbackID:command.callbackId withMessage:[@"推流失败,code:" stringByAppendingString: code]];   
    }else{
        [self successWithCallbackID:command.callbackId withMessage:@"success"];
    }
}
- (void) pausePusher:(CDVInvokedUrlCommand*)command{
    if (self.livePusher) return;
    [self.livePusher pausePush];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}
- (void) resumePusher:(CDVInvokedUrlCommand*)command{
    if (self.livePusher) return;
    [self.livePusher resumePush];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) stopPush:(CDVInvokedUrlCommand*)command {
    if (!self.livePusher) return;
    [self.livePusher stopPreview];
    [self.livePusher stopPush];
    self.livePusher.delegate = nil;
    self.livePusher = nil;
    UIApplication* app = [UIApplication sharedApplication];
  if([app isIdleTimerDisabled]) {
    [app setIdleTimerDisabled:false];
  }
    [self destroyVideoView];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) startPlay:(CDVInvokedUrlCommand*)command {
    if (self.livePlayer) return;
    NSString* url = [command.arguments objectAtIndex:0];
//    TX_Enum_PlayType playUrlType = (TX_Enum_PlayType)[command.arguments objectAtIndex:1];
//    NSInteger playUrlType = (NSInteger)[command.arguments objectAtIndex:1];

    [self prepareVideoView];

    self.livePlayer = [[TXLivePlayer alloc] init];
    [self.livePlayer setupVideoWidget:CGRectMake(0, 0, 0, 0) containView:videoView insertIndex:0];
    [self.livePlayer startPlay:url type:PLAY_TYPE_LIVE_FLV];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) stopPlay:(CDVInvokedUrlCommand*)command {
    if (!self.livePlayer) return;
    [self.livePlayer stopPlay];
    [self.livePlayer removeVideoWidget];
    self.livePlayer = nil;
    [self destroyVideoView];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) setVideoQuality:(CDVInvokedUrlCommand*)command{
    if (!self.livePusher) return;
    NSInteger quality =[[command.arguments objectAtIndex:0] integerValue];
    
    NSInteger enabled = [[command.arguments objectAtIndex:1] integerValue];
    BOOL isEnabled = NO;
    if(enabled == 1){
        isEnabled = YES;
    }
    //(void)setVideoQuality:(TX_Enum_Type_VideoQuality)quality
    //   adjustBitrate:(BOOL) adjustBitrate
    //adjustResolution:(BOOL) adjustResolution;
    [self.livePusher setVideoQuality:(TX_Enum_Type_VideoQuality)quality adjustBitrate:enabled adjustResolution:NO];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) setBeautyFilterDepth:(CDVInvokedUrlCommand*)command{
    if (!self.livePusher) return;
    NSInteger beautyStyle = [[command.arguments objectAtIndex:0] integerValue];
    NSNumber* beautyLevel = [command.arguments objectAtIndex:1];
    NSNumber* whitenessLevel = [command.arguments objectAtIndex:2];
    NSNumber* ruddinessLevel = [command.arguments objectAtIndex:3];
    /**
     setBeautyStyle:(TX_Enum_Type_BeautyStyle)beautyStyle
        beautyLevel:(float)beautyLevel
     whitenessLevel:(float)whitenessLevel
     ruddinessLevel:(float)ruddinessLevel TX_DEPRECAETD_BEAUTY_API;
     */
    [self.livePusher setBeautyStyle:(TX_Enum_Type_BeautyStyle)beautyStyle beautyLevel:[beautyLevel floatValue] whitenessLevel:[whitenessLevel floatValue] ruddinessLevel:[ruddinessLevel floatValue]];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) setFilter:(CDVInvokedUrlCommand*)command{
    return;
}
- (void) switchCamera:(CDVInvokedUrlCommand*)command{
    if (!self.livePusher) return;
    [self.livePusher switchCamera];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) setMirror:(CDVInvokedUrlCommand*)command{
    if (!self.livePusher) return;
    NSInteger enabled = [[command.arguments objectAtIndex:0] integerValue];
    BOOL isEnabled = NO;
    if(enabled == 1){
        isEnabled = YES;
    }
    [self.livePusher setMirror:isEnabled];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) turnOnFlashLight:(CDVInvokedUrlCommand*)command{
    if (!self.livePusher) return;
    NSInteger enabled = [[command.arguments objectAtIndex:0] integerValue];
    BOOL isEnabled = NO;
    if(enabled == 1){
        isEnabled = YES;
    }
    [self.livePusher toggleTorch:isEnabled];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}

- (void) getMaxZoom:(CDVInvokedUrlCommand*)command{
    if ( command ) {
        if (!self.livePusher) return;
        [self successWithCallbackID:command.callbackId withMessage:@"5"];
    }
}
- (void) setZoom:(CDVInvokedUrlCommand*)command{
    if (!self.livePusher) return;
    NSNumber* zoom = [command.arguments objectAtIndex:0];
    [self.livePusher setZoom:[zoom floatValue]];
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
}
- (void) setExposureCompensation:(CDVInvokedUrlCommand*)command{
    [self successWithCallbackID:command.callbackId withMessage:@"success"];
    return;
}

- (void) alert:(NSString*)message title:(NSString*)title {
    UIAlertView* alert = [
                          [UIAlertView alloc]
                          initWithTitle:title
                          message:message
                          delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil
                          ];
    [alert show];
    //[alert release];
}

- (void)  alert:(NSString*)message {
    [self alert:message title:@"系统消息"];
}

- (void)successWithCallbackID:(NSString *)callbackID
{
    [self successWithCallbackID:callbackID withMessage:@"OK"];
}

- (void)successWithCallbackID:(NSString *)callbackID withMessage:(NSString *)message
{
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:message];
    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackID];
}

- (void)failWithCallbackID:(NSString *)callbackID withError:(NSError *)error
{
    [self failWithCallbackID:callbackID withMessage:[error localizedDescription]];
}

- (void)failWithCallbackID:(NSString *)callbackID withMessage:(NSString *)message
{
    CDVPluginResult *commandResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:message];
    [self.commandDelegate sendPluginResult:commandResult callbackId:callbackID];
}

@end
