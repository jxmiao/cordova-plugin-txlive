/*global cordova, module*/

module.exports = {

    // 设置最小字号以解决兼容 BUG
    fixMinFontSize: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "fixMinFontSize", []);
    },

    // 推流类方法
    //推流
    startPush: function(url, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "startPush", [url]);
    },
    // 暂停
    pausePusher: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "pausePusher", []);
    },
    //恢复
    resumePusher: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "resumePusher", []);
    },
    //关闭
    stopPush: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "stopPush", []);
    },

    // 播放类方法
    PLAY_URL_TYPE: {
        PLAY_TYPE_LIVE_RTMP:     0, // 传入的URL为RTMP直播地址
        PLAY_TYPE_LIVE_FLV:      1, // 传入的URL为FLV直播地址
        PLAY_TYPE_VOD_FLV:       2, // 传入的URL为RTMP点播地址
        PLAY_TYPE_VOD_HLS:       3, // 传入的URL为HLS(m3u8)点播地址
        PLAY_TYPE_VOD_MP4:       4, // 传入的URL为MP4点播地址
        PLAY_TYPE_LIVE_RTMP_ACC: 5, // 低延迟连麦链路直播地址（仅适合于连麦场景）
        PLAY_TYPE_LOCAL_VIDEO:   6  // 手机本地视频文件
    },
    //播放
    startPlay: function(url, playUrlType, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "startPlay", [url, playUrlType]);
    },
    //停止播放
    stopPlay: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "stopPlay", []);
    },

    // 调整类方法

    // 推流
    // 设定观看端清晰度 
    //quality 1 标清; 2 高清; 3 超清;
    //enabled 是否根据网络动态调整画质 1 弱网降画质 0保持画质
    setVideoQuality: function(quality, enabled,
                              successCallback, errorCallback) {
        cordova.exec(
            successCallback, errorCallback, "TencentLive", "setVideoQuality",
            [quality, enabled]
        );
    },

    // 推流
    // 设定美颜级别
    ///style             美颜算法：  0：光滑  1：自然  2：朦胧    
    //beautyLevel       磨皮等级： 取值为 0-9.取值为 0 时代表关闭美颜效果.默认值: 0,即关闭美颜效果.    
    //whiteningLevel    美白等级： 取值为 0-9.取值为 0 时代表关闭美白效果.默认值: 0,即关闭美白效果.    
    //ruddyLevel        红润等级： 取值为 0-9.取值为 0 时代表关闭美白效果.默认值: 0,即关闭美白效果.  
    setBeautyFilterDepth: function(style, beautyLevel, whiteningLevel, ruddyLevel, successCallback, errorCallback) {
        cordova.exec(
            successCallback, errorCallback, "TencentLive", "setBeautyFilterDepth",
            [style, beautyLevel, whiteningLevel, ruddyLevel]
        );
    },

    // 推流
    // 切换前后置镜头，调用一次切换一次
    // callback 返回调用之后的镜头是 front 还是 back
    switchCamera: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "switchCamera", []);
    },

    // 推流
    //镜像 1：播放端看到的是镜像画面；0：播放端看到的是非镜像画面。
    setMirror: function(enabled, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "setMirror", [enabled]);
    },

    // 推流
    // 开启/关闭闪光灯 1开启 0 关闭
    turnOnFlashLight: function(enabled, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "turnOnFlashLight", [enabled]);
    },
    //获取最大变焦值，collback 返回值
    getMaxZoom: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "setMirror", []);
    },

    // 推流
    // 设置手动对焦（1-getMaxZoom）
    setZoom: function(zoom, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "TencentLive", "setZoom", [zoom]);
    },

    // 推流
    // 设定曝光级别
    // depth: -1 ~ 1 的浮点数
    setExposureCompensation: function(depth, successCallback, errorCallback) {
        cordova.exec(
            successCallback, errorCallback, "TencentLive", "setExposureCompensation",
            [depth]
        );
    },
    // 推流
    // 设定滤镜 暂时无法实现
    setFilter: function(filterUrl, successCallback, errorCallback) {
        cordova.exec(
            successCallback, errorCallback, "TencentLive", "setFilter",
            [filterUrl]
        );
    },

};
